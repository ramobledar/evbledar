import { Drug, PatientsRegister } from "./patientsRegister";
export declare class Quarantine {
    private readonly patients;
    private drugs;
    constructor(patients: PatientsRegister);
    setDrugs(drugs: Array<Drug>): void;
    wait40Days(): void;
    report(): PatientsRegister;
}
