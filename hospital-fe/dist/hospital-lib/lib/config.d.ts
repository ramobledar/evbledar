export declare const PATIENT_MAP: {
    [key: string]: string;
};
export declare const DRUGS_MAP: {
    [key: string]: string;
};
export declare const DRUG_RULES: {
    F: Map<any, any>;
    H: Map<any, any>;
    D: Map<any, any>;
    T: Map<any, any>;
    X: Map<any, any>;
};
