import { Affliction, Drug } from "./patientsRegister";
export declare function getDrugCombo(drugs: any): any;
export declare function drugFormula(affliction: Affliction, drugs: Array<Drug | null>): Affliction;
export declare function patientNewState(patientsKeyValue: any, patientsRegisterUpdates: any, drugs: any): any;
