export interface PatientsRegister {
    [key: string]: number;
}
export declare type Drug = 'As' | 'An' | 'I' | 'P';
export declare type Affliction = 'F' | 'H' | 'D' | 'T' | 'X';
