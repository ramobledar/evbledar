import * as i0 from '@angular/core';
import { NgModule } from '@angular/core';

const PATIENT_MAP = {
    F: 'Fever',
    H: 'Healthy',
    D: 'Diabetic',
    T: 'Tuberculosis',
    X: 'Dead',
};
const DRUGS_MAP = {
    '': 'None',
    As: 'Aspirin',
    An: 'Antibiotic',
    I: 'Insulin',
    P: 'Paracetamol',
    'An,I': 'Antibiotic + Insulin',
    'As,P': 'Paracetamol + Aspirin',
};
const DRUG_RULES = {
    F: new Map().set('As', 'H').set('P', 'H').set('As,P', 'X'),
    H: new Map().set('An,I', 'F').set('As,P', 'X'),
    D: new Map().set(null, 'X').set('I', 'D').set('As,P', 'X'),
    T: new Map().set('An', 'H').set('As,P', 'X'),
    X: new Map(),
};

class HospitalLibModule {
}
HospitalLibModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.13", ngImport: i0, type: HospitalLibModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
HospitalLibModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "12.2.13", ngImport: i0, type: HospitalLibModule });
HospitalLibModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "12.2.13", ngImport: i0, type: HospitalLibModule, imports: [[]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.13", ngImport: i0, type: HospitalLibModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [],
                    imports: [],
                    exports: []
                }]
        }] });

function getDrugCombo(drugs) {
    const sortedDrugs = drugs.sort();
    return [].concat(sortedDrugs)
        .concat(sortedDrugs.filter(Boolean).join(','))
        .filter((d) => d !== '');
}
function drugFormula(affliction, drugs) {
    const next = getDrugCombo(drugs)
        .map((d) => DRUG_RULES[affliction].get(d))
        .filter(Boolean);
    if (next.includes('X')) {
        return 'X';
    }
    else {
        const survivingAfflictions = next.filter((a) => a !== 'X');
        const survivingAfflictionsLength = survivingAfflictions.length;
        const end = survivingAfflictionsLength > 0
            ? survivingAfflictions[survivingAfflictionsLength - 1]
            : null;
        return end || DRUG_RULES[affliction].get(null) || affliction;
    }
}
function patientNewState(patientsKeyValue, patientsRegisterUpdates, drugs) {
    return patientsKeyValue.reduce((patients, entry) => {
        const [disease, count] = entry;
        patients[disease] = patients[disease] || 0;
        const drugsExist = drugs ? drugs : [null];
        const nextAffliction = drugFormula(disease, drugsExist);
        if (disease !== nextAffliction) {
            patients[disease] = patients[disease] - count;
            patients[nextAffliction] = patients[nextAffliction] + count;
        }
        return patients;
    }, patientsRegisterUpdates);
}

class Quarantine {
    constructor(patients) {
        this.patients = Object.assign({}, patients);
    }
    setDrugs(drugs) {
        this.drugs = drugs;
    }
    wait40Days() {
        let patientsKeyValue = Object.entries(this.patients);
        const patientsRegisterUpdates = Object.keys(this.patients)
            .reduce((acc, key) => {
            acc[key] = 0;
            return acc;
        }, {});
        const newPatientState = patientNewState(patientsKeyValue, patientsRegisterUpdates, this.drugs);
        for (const patient in this.patients) {
            const count = newPatientState[patient] || 0;
            this.patients[patient] = this.patients[patient] + count;
        }
    }
    report() {
        return this.patients;
    }
}

/*
 * Public API Surface of hospital-lib
 */

/**
 * Generated bundle index. Do not edit.
 */

export { DRUGS_MAP, DRUG_RULES, HospitalLibModule, PATIENT_MAP, Quarantine };
//# sourceMappingURL=hospital-lib.js.map
