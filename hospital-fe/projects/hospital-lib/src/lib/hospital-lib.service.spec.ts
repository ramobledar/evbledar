import { TestBed } from '@angular/core/testing';

import { HospitalLibService } from './hospital-lib.service';

describe('HospitalLibService', () => {
  let service: HospitalLibService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HospitalLibService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
