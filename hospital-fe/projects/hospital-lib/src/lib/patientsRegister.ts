
export interface PatientsRegister {
  [key: string]: number
}

export type Drug =
  'As'      // Aspirin
  | 'An'    // Antibiotic
  | 'I'     // Insulin
  | 'P'     // Paracetamol

export type Affliction =
  'F'       // Fever
  | 'H'     // Healthy
  | 'D'     // Diabetes
  | 'T'     // Tuberculosis
  | 'X'     // Dead
