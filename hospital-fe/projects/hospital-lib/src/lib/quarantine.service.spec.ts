import { Quarantine } from './quarantine.service';
import {PatientsRegister} from "./patientsRegister";

describe('Quarantine', () => {
  let quarantineService: Quarantine;

  beforeEach(() => {
    quarantineService = new Quarantine({
      F: 1, H: 2, D: 3, T: 1, X: 0
    } as PatientsRegister)
  });

  it('beforeTreatment', () => {
    expect(quarantineService.report()).toEqual({
      F: 1, H: 2, D: 3, T: 1, X: 0
    });
  });

  it('noTreatment', () => {
    quarantineService.wait40Days();
    expect(quarantineService.report()).toEqual({
      F: 1, H: 2, D: 0, T: 1, X: 3
    });
  });

  it('aspirin', () => {
    quarantineService.setDrugs(['As']);
    quarantineService.wait40Days();
    expect(quarantineService.report()).toEqual({
      F: 0, H: 3, D: 0, T: 1, X: 3
    });
  });

  it('antibiotic', () => {
    quarantineService.setDrugs(['An']);
    quarantineService.wait40Days();
    expect(quarantineService.report()).toEqual({
      F: 1, H: 3, D: 0, T: 0, X: 3
    });
  });

  it('insulin', () => {
    quarantineService.setDrugs(['I']);
    quarantineService.wait40Days();
    expect(quarantineService.report()).toEqual({
      F: 1, H: 2, D: 3, T: 1, X: 0
    });
  });

  it('antibioticPlusInsulin', () => {
    quarantineService.setDrugs(['An', 'I']);
    quarantineService.wait40Days();
    expect(quarantineService.report()).toEqual({
      F: 3, H: 1, D: 3, T: 0, X: 0
    });
  });

  it('paracetamol', () => {
    quarantineService.setDrugs(['P']);
    quarantineService.wait40Days();
    expect(quarantineService.report()).toEqual({
      F: 0, H: 3, D: 0, T: 1, X: 3
    });
  });

  it('paracetamolAndAspirin', () => {
    quarantineService.setDrugs(['P', 'As']);
    quarantineService.wait40Days();
    expect(quarantineService.report()).toEqual({
      F: 0, H: 0, D: 0, T: 0, X: 7
    });
  });

});
