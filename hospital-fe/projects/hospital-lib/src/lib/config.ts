export const PATIENT_MAP: { [key: string]: string } = {
    F: 'Fever',
    H: 'Healthy',
    D: 'Diabetic',
    T: 'Tuberculosis',
    X: 'Dead',
};

export const DRUGS_MAP: { [key: string]: string } = {
    '': 'None',
    As: 'Aspirin',
    An: 'Antibiotic',
    I: 'Insulin',
    P: 'Paracetamol',
    'An,I': 'Antibiotic + Insulin',
    'As,P': 'Paracetamol + Aspirin',
};

export const DRUG_RULES = {
  F: new Map().set('As', 'H').set('P', 'H').set('As,P', 'X'),
  H: new Map().set('An,I', 'F').set('As,P', 'X'),
  D: new Map().set(null, 'X').set('I', 'D').set('As,P', 'X'),
  T: new Map().set('An', 'H').set('As,P', 'X'),
  X: new Map(),
};
