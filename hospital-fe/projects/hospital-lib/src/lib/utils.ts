import {Affliction, Drug} from "./patientsRegister";
import {DRUG_RULES} from "./config";

export function getDrugCombo(drugs: any): any {
  const sortedDrugs = drugs.sort();
  return [].concat(sortedDrugs)
    .concat(sortedDrugs.filter(Boolean).join(','))
    .filter((d: any) => d !== '');
}

export function drugFormula(
  affliction: Affliction,
  drugs: Array<Drug | null>
): Affliction {

  const next = getDrugCombo(drugs)
    .map((d: any) => DRUG_RULES[affliction].get(d))
    .filter(Boolean)

  if (next.includes('X')) {
    return 'X';
  } else {
    const survivingAfflictions = next.filter((a: any) => a !== 'X');
    const survivingAfflictionsLength = survivingAfflictions.length

    const end =
      survivingAfflictionsLength > 0
        ? survivingAfflictions[survivingAfflictionsLength - 1]
        : null;

    return end || DRUG_RULES[affliction].get(null) || affliction;
  }
}

export function patientNewState(patientsKeyValue:any,patientsRegisterUpdates:any, drugs:any) {
  return patientsKeyValue.reduce(
    (patients: any, entry: any) => {
      const [disease, count] = entry;
      patients[disease] = patients[disease] || 0;
      const drugsExist = drugs ? drugs : [null];
      const nextAffliction = drugFormula(disease, drugsExist);
      if (disease !== nextAffliction) {
        patients[disease] = patients[disease] - count;
        patients[nextAffliction] = patients[nextAffliction] + count;
      }
      return patients;
    },
    patientsRegisterUpdates
  );
}
