import {Affliction, Drug, PatientsRegister} from "./patientsRegister";
import {patientNewState} from "./utils";


export class Quarantine {

  private readonly patients: PatientsRegister;
  private drugs: Array<Drug> | undefined;

  constructor(patients: PatientsRegister) {
    this.patients = { ...patients };
  }

  public setDrugs(drugs: Array<Drug>): void {
    this.drugs = drugs;
  }

  public wait40Days(): void {
    let patientsKeyValue = Object.entries(this.patients);
    const patientsRegisterUpdates =  Object.keys(this.patients)
      .reduce((acc:any , key: string ) => {
        acc[key] = 0; return acc; }, {})
    const newPatientState = patientNewState(patientsKeyValue,patientsRegisterUpdates, this.drugs)
    for (const patient in this.patients) {
      const count = newPatientState[<Affliction>patient] || 0;
      this.patients[<Affliction>patient] = this.patients[<Affliction>patient] + count;
    }
  }

  public report(): PatientsRegister {
    return this.patients;
  }
}
