/*
 * Public API Surface of hospital-lib
 */

export * from './lib/config';
export * from './lib/hospital-lib.module';
export {Quarantine} from './lib/quarantine.service';
