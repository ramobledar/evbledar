import { TestBed } from '@angular/core/testing';
import { HospitalProviderService } from './hospital.provider.service';

describe('HospitalProviderService', () => {
  let service: HospitalProviderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HospitalProviderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
