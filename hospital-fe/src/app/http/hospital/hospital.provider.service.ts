import {Injectable} from '@angular/core';
import {map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {forkJoin, Observable, throwError} from 'rxjs';
import {PatientsRegister, ServiceResponse} from "../../shared/models";

@Injectable({
  providedIn: 'root'
})
export class HospitalProviderService {

  private apiUrl = 'http://localhost:7200';

  constructor(private http: HttpClient) {
  }

  hospitalData(param: string): Observable<ServiceResponse<string>> {
    return this.http.get<any>(`${this.apiUrl}/${param}`)
  }

  joinPatientsAndDrugs() :Observable<[PatientsRegister, []]>{
    return forkJoin([
      this.hospitalData('patients')
        .pipe(map(
          (response:ServiceResponse<string>) => {
            return response.toString()
              .split(',')
              .reduce<PatientsRegister>((prev: any, cur: string) => {
                prev[cur] = (prev[cur] || 0) + 1;
                return prev;
              }, {F: 0, H: 0, D: 0, T: 0, X: 0})
          })),
      this.hospitalData('drugs').pipe(map(
        (response:ServiceResponse<string>) => {
          return response.toString().split(',') as [];
        }))])
  }

}
