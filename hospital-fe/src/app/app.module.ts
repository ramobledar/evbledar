import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HospitalLibModule} from "hospital-lib";
import {PatientAndDrugsComponent} from './features/drugs/patient-and-drugs/patient-and-drugs.component';
import {DrugSimulatorComponent} from './features/drugs/drug-simulator/drug-simulator.component';
import {StoreModule} from "@ngrx/store";
import {EffectsModule} from "@ngrx/effects";
import {PatientEffects} from "./store/patient.effects";
import {StoreDevtoolsModule} from "@ngrx/store-devtools";
import {SharedModule} from "./shared/shared.module";
import {fromApp} from "./store";

@NgModule({
  declarations: [AppComponent, PatientAndDrugsComponent, DrugSimulatorComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    StoreModule.forRoot({[fromApp.appFeatureKey]: fromApp.reducer}),
    EffectsModule.forRoot([PatientEffects]),
    StoreDevtoolsModule.instrument({maxAge: 25}),
    BrowserAnimationsModule,
    HospitalLibModule,
    SharedModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {
}
