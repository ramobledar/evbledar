import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  template: `<app-patient-and-drugs></app-patient-and-drugs>`,
  styleUrls: ['./app.component.css'],
})


export class AppComponent {

}
