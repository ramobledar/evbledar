import {Injectable} from '@angular/core';
import {DrugsAndPatient, PatientsRegister, TableRows} from "../../shared/models";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ValidateDrugsCombination, ValidateDrugsSelectedAmount} from "../../shared/validators";
import {getKeyByValue, getKeyByValueArray} from "../../shared/utils";
import {DRUGS_MAP, PATIENT_MAP} from "../../../../projects/hospital-lib/src/lib/config";
import { Drug } from '../../../../projects/hospital-lib/src/lib/patientsRegister';
import { Quarantine } from 'projects/hospital-lib/src/public-api';

@Injectable({
  providedIn: 'root'
})
export class SimulatorService {
  reports: any = [];

  constructor(
    private fb: FormBuilder,
  ) {

  }

  simulateAndReport = (patientRegister: PatientsRegister, drugs: Drug[], index: number) => {

    // @ts-ignore
    const simulator = new Quarantine(patientRegister);
    simulator.setDrugs(drugs);
    simulator.wait40Days();
    this.addReport({
      key: index,
      drugs,
      result: simulator.report(),
      prior: patientRegister,
    });
    return this.reports;
  };

  addReport = (report: any) => {
    this.reports = [report, ...this.reports].filter((x) => !!x);
  };

  getReports() {
    return this.reports
  }

  drugFormSimulator(patient: any, drugs: any): FormGroup {
    return this.fb.group({
      drugs: [drugs.drugs, [Validators.required, ValidateDrugsCombination(), ValidateDrugsSelectedAmount(1)]],
      Fever: [patient.patientRegister['F'], [Validators.required, Validators.min(0), Validators.pattern("^[0-9]*$"),]],
      Healthy: [patient.patientRegister['H'], [Validators.required, Validators.min(0), Validators.pattern("^[0-9]*$"),]],
      Diabetic: [patient.patientRegister['D'], [Validators.required, Validators.min(0), Validators.pattern("^[0-9]*$"),]],
      Tuberculosis: [patient.patientRegister['T'], [Validators.required, Validators.min(0), Validators.pattern("^[0-9]*$"),]],
      Dead: [patient.patientRegister['X'], [Validators.required, Validators.min(0), Validators.pattern("^[0-9]*$"),]],
    });
  }

  mapPatientDrugs(row: TableRows, index: number, rawDataPatientDrugs: Array<DrugsAndPatient>) {
    let copy = rawDataPatientDrugs;
    Object.keys(row).forEach((key: string) => {
      let patientKeyValue = getKeyByValue(PATIENT_MAP, key);
      let drugsKeyValue = getKeyByValueArray(DRUGS_MAP, row.drugs)

      if (patientKeyValue) {
        let patientKeyValueString = patientKeyValue.toString()
        copy[index].patientRegister = {
          ...copy[index].patientRegister,
          // @ts-ignore
          [patientKeyValueString]: row[key]
        }
        if (drugsKeyValue) {
          copy[index].drugs = drugsKeyValue
        }
      }
    });
    return copy;
  }

  filterDrugsMapDropdown(){
    return Object.keys(DRUGS_MAP).reduce((p:any, c) => {
      if (!DRUGS_MAP[c].includes("+")) p[c] = DRUGS_MAP[c];
      return p;
    }, {});
  }
}
