import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DrugSimulatorComponent } from './drug-simulator.component';

describe('DrugSimulatorComponent', () => {
  let component: DrugSimulatorComponent;
  let fixture: ComponentFixture<DrugSimulatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DrugSimulatorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DrugSimulatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
