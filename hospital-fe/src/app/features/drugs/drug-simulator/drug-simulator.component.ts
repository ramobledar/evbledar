import {Component, Input, OnInit} from '@angular/core';
import {HistoricalData, Report} from "../../../shared/models";
import {MatTableDataSource} from "@angular/material/table";
import {DRUGS_MAP} from "../../../../../projects/hospital-lib/src/lib/config";
import {rowsAnimation} from "../../../animations/template.animations";

@Component({
  selector: 'app-drug-simulator',
  templateUrl: './drug-simulator.component.html',
  styleUrls: ['./drug-simulator.component.css'],
  animations: [rowsAnimation],
})
export class DrugSimulatorComponent {

  @Input() simulationRows: Array<Report> = [];
  @Input() parentOrder: number = 0;

  displayedColumns = ['key', 'Drugs', 'Fever', 'Healthy', 'Diabetic', 'Tuberculosis', 'Dead'];
  dataSource: MatTableDataSource<HistoricalData>;

  constructor() {
    const initData: HistoricalData[] = [];
    this.dataSource = new MatTableDataSource(initData);
  }

  ngOnChanges() {
    this.dataSource = new MatTableDataSource(this.simulationRows.map((row: any, index: number) => {
      return {
        key: row.key,
        Drugs: row.drugs.map((d: string) => DRUGS_MAP[d]),
        Diabetic: row.prior['D'] + '>' + row.result['D'],
        Fever: row.prior['F'] + ' > ' + row.result['F'],
        Healthy: row.prior['H'] + '>' + row.result['H'],
        Tuberculosis: row.prior['T'] + '>' + row.result['T'],
        Dead: row.prior['X'] + ' >' + row.result['X']
      }
    }).filter(t => {
      return Number(t.key) === this.parentOrder
    }));
  }
}
