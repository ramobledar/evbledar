import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientAndDrugsComponent } from './patient-and-drugs.component';

describe('PatientAndDrugsComponent', () => {
  let component: PatientAndDrugsComponent;
  let fixture: ComponentFixture<PatientAndDrugsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientAndDrugsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientAndDrugsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
