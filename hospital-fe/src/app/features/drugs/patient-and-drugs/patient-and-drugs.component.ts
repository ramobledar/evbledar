import {Component, OnInit} from '@angular/core';
import {select, Store} from "@ngrx/store";
import {selectAppUsers, selectUsersPageError} from "../../../store/patient.selector";
import {AppActions, fromApp} from "../../../store";
import {DrugsAndPatient, FormTableRow, Report, TableRows} from "../../../shared/models";
import {FormArray, FormBuilder, FormGroup} from "@angular/forms";
import {automaticRefresh, clearFormArray} from "../../../shared/utils";
import {DRUGS_MAP, PATIENT_MAP} from '../../../../../projects/hospital-lib/src/lib/config';
import {SimulatorService} from "../simulator.service";
import {delay, last, takeLast, tap} from "rxjs/operators";

@Component({
  selector: 'app-patient-and-drugs',
  templateUrl: './patient-and-drugs.component.html',
  styleUrls: ['./patient-and-drugs.component.css']
})
export class PatientAndDrugsComponent implements OnInit {

  DRUGS_MAP: { [key: string]: string } = DRUGS_MAP
  PATIENT_MAP: { [key: string]: string } = PATIENT_MAP;
  patients$ = this.store.pipe(select(selectAppUsers));
  patientsError$ = this.store.pipe(select(selectUsersPageError));
  beforeAndAfterReports: Array<Report> = [];

  drugsFormTableSimulator: FormGroup = new FormGroup({
    tableRows: this.fb.array([])
  });

  mappedPatientDrugs: Array<DrugsAndPatient> = [];

  constructor(
    private fb: FormBuilder,
    private store: Store<fromApp.AppState>,
    private simulatorService: SimulatorService
  ) {
  }

  initialPatient(): void {
    this.store.dispatch(AppActions.fetchPatients());
    this.patients$.subscribe((allResults: any) => {

      // console.log(this.mappedPatientDrugs.length)
      if (allResults?.length) {
        (<FormArray>this.drugsFormTableSimulator.get('tableRows')).push(this.simulatorService.drugFormSimulator({patientRegister: allResults[0]}, {drugs: allResults[1].map((d: string) => DRUGS_MAP[d])}));
        this.mappedPatientDrugs.push({patientRegister: allResults[0], drugs: allResults[1]})
      }


      // console.log(this.mappedPatientDrugs.length)

    })
  }

  ngOnInit(): void {
    this.initialPatient()
    this.onDrugsValueChanges();
    this.DRUGS_MAP = this.simulatorService.filterDrugsMapDropdown()

    this.getFormControls.valueChanges.subscribe(dt=>{
      this.deleteExpense()
    })
   // console.log('aaaaaaaa')
   //  this.patients$.pipe(takeLast(10)).subscribe(dt=>{
   //    console.log(dt)
   //  })
   //  console.log('bbbb')
  }

  onDrugsValueChanges(): void {
    this.drugsFormTableSimulator.valueChanges.subscribe((updatedValue: FormTableRow) => {
      updatedValue.tableRows.map((row: TableRows, index: number) => {
        if (this.mappedPatientDrugs[index]) {
          this.mappedPatientDrugs = this.simulatorService.mapPatientDrugs(row, index, this.mappedPatientDrugs)
        }
      })
    })
  }

  get getFormControls(): FormArray {
    return this.drugsFormTableSimulator.get('tableRows') as FormArray;
  }

  deleteExpense() {
    if (this.getFormControls.length > 11)
      this.getFormControls.removeAt(0);
  }


  retrievePatientAndDrugs(): void {
    this.store.dispatch(AppActions.fetchPatients());
  }

  autoRetrievePatientAndDrugs(): void {
    clearFormArray(this.drugsFormTableSimulator.get('tableRows') as FormArray)
    this.mappedPatientDrugs = [];
    this.beforeAndAfterReports = [];
    automaticRefresh(1000, 11).pipe(
      tap(() => this.store.dispatch(AppActions.fetchPatients())),
      delay(2000)).subscribe((index: number) => {
      this.beforeAndAfterReports = this.simulatorService.simulateAndReport(this.mappedPatientDrugs[index].patientRegister, [...this.mappedPatientDrugs[index].drugs], index)

      })
  }

  deleteRow(index: number) {
  console.log(index)
    // if(this.mappedPatientDrugs.length === 10){
    //   console.log(this.mappedPatientDrugs.length)
    //   let removeThisIncex = this.mappedPatientDrugs.length - 10
    //   this.deleteRow(0)
    // }
    const control = this.drugsFormTableSimulator.get('tableRows') as FormArray;
    control.removeAt(index);
    this.mappedPatientDrugs.splice(index, 1);
  }

  runSimulator(i: number) {
    this.beforeAndAfterReports = this.simulatorService.simulateAndReport(this.mappedPatientDrugs[i].patientRegister, [...this.mappedPatientDrugs[i].drugs], i)
  }
}
