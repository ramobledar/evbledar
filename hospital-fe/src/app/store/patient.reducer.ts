import {Action, createReducer, on} from '@ngrx/store';
import {AppActions} from "./index";

export const appFeatureKey = 'app';

export interface AppState {
  users: any;
  error: any;
}

export const initialState: AppState = {
  users: null,
  error: null,
};

export const appReducer = createReducer(
  initialState,
  on(AppActions.loadPatientsSuccess, (state, action) => {
    console.log(action)
    return {...state, users: action.data, error: null};
  }),
  on(AppActions.loadPatientsFailure, (state, action) => {
    return {...state, error: action.error};
  }),
);


export function reducer(state: AppState | undefined, action: Action): AppState {
  return appReducer(state, action);
}
