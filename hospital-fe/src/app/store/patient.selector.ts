import { createFeatureSelector, createSelector } from '@ngrx/store';
import {fromApp} from "./index";
import {takeLast} from "rxjs/operators";


export const selectAppState = createFeatureSelector<fromApp.AppState>(
  fromApp.appFeatureKey
);

export const selectUsersPageError = createSelector(
  selectAppState,
  state => state.error // return error message
)

export const selectAppUsers = createSelector(
  selectAppState,
  (state: fromApp.AppState) => {
    return state.users
  }
);
