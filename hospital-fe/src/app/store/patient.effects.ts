import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {HospitalProviderService} from "../http/hospital/hospital.provider.service";
import {AppActions} from "./index";
import {
  catchError,
  map,
  switchMap,
  withLatestFrom
} from 'rxjs/operators';
import {Store} from "@ngrx/store";
import {AppState} from "./patient.reducer";
import {selectAppUsers} from "./patient.selector";
import {of} from "rxjs";

@Injectable()
export class PatientEffects {
  private isPollingActive: boolean = false;


  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private dataProviderService: HospitalProviderService
  ) {
  }

  // Example 1
  public onFetchUser$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppActions.fetchPatients),
      withLatestFrom(this.store.select(selectAppUsers)),
      switchMap(() =>
        this.dataProviderService.joinPatientsAndDrugs().pipe(
          map((data: any) => {
            return AppActions.loadPatientsSuccess({data});
          }),
          catchError((error) => of(AppActions.loadPatientsFailure({error})))
        )
      )
    );
  });

}
