import * as AppActions from './patient.actions';
import * as fromApp from './patient.reducer';

export {AppActions};
export {fromApp};
