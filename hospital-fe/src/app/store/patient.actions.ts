import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';


export const loadPatientsFailure = createAction(
  '[Patient and Drugs API] Load Failure',
  props<{ error: HttpErrorResponse }>()
);

export const loadPatientsSuccess = createAction(
  '[Patient and Drugs API] Load Success',
  props<{ data: any[] }>()
);

export const fetchPatients = createAction(
  '[User API] Fetch Users Success'
);
