import {FormArray} from "@angular/forms";
import {take} from "rxjs/operators";
import {Observable} from 'rxjs';
import { interval } from 'rxjs';
import {DRUGS_MAP, PATIENT_MAP} from "../../../projects/hospital-lib/src/lib/config";
import {TableRows} from "./models";

export const clearFormArray = (formArray: FormArray) => {
  while (formArray.length !== 0) {
    formArray.removeAt(0)
  }
}

export const getKeyByValue = (object: any, value: string) => {
  return Object.keys(object).find((key: string) => object[key] === value);
}

export const getKeyByValueArray = (object: any, array: [string]) => {
  return Object.keys(object).filter(key => array.includes(object[key]));
}

export const automaticRefresh = (intervalsec:any,repetitions:number):Observable<number> => {
  return interval(intervalsec).pipe(take(repetitions));
}

export const UPDATE = (row:TableRows,index:number,rawDataPatientDrugs:Array<any>)=>{
  Object.keys(row).forEach((key) => {
    let patientKeyValue = getKeyByValue(PATIENT_MAP, key);
    if (patientKeyValue) {
      let patientKeyValueString = patientKeyValue.toString()
      rawDataPatientDrugs[index].patientRegister = {
        ...rawDataPatientDrugs[index].patientRegister,
        // @ts-ignore
        [patientKeyValueString]: row[key]
      }
      let drugsKeyValue  = getKeyByValueArray(DRUGS_MAP,row.drugs)
      if(drugsKeyValue){
        rawDataPatientDrugs[index]['drugs'] = drugsKeyValue
      }
    }
  });
}
