import {HttpErrorResponse} from "@angular/common/http";
import {Affliction} from "../../../projects/hospital-lib/src/lib/patientsRegister";

export interface TableRowsItems extends Array<TableRows> {
}

export interface ServiceResponse<T> {
  success: boolean;
  error?: HttpErrorResponse;
  data?: [PatientsRegister, []];
}

export interface Report {
  drugs: [string];
  key: number;
  prior: object,
  result: object
};

export interface DrugsAndPatient {
  patientRegister: PatientsRegister;
  drugs: Array<any>
};


export interface HistoricalData {
  Drugs: string;
  key: number;
  Fever: string;
  Healthy: string;
  Diabetic: string;
  Tuberculosis: string;
  Dead: string;
}


export interface TableRows {
  key?: string;
  index?: string;
  drugs: [string];
  Fever: string;
  Healthy: string;
  Diabetic: string;
  Tuberculosis: string;
  Dead: string;
}

export interface FormTableRow {
  tableRows: TableRowsItems
}

export type PatientsRegister = {
  [key in Affliction]?: number
}
