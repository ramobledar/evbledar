import {AbstractControl, ValidationErrors, ValidatorFn} from "@angular/forms";
import {DRUGS_MAP} from "../../../projects/hospital-lib/src/lib/config";

export function ValidateDrugsCombination(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    let drugsArray  = Object.values(DRUGS_MAP)
    let value = control.value;

    if(value.length > 1){
      return  drugsArray.includes(value.join(" + ")) ||
      drugsArray.includes(value.reverse().join(" + ")) ?
        null: { invalidDrugs: true };
    } else {
      let res = drugsArray.includes(value.toString())
      return res ? null :{ invalidDrugs: true }
    }
  };
}


export function ValidateDrugsSelectedAmount(amount:number): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    let value = control.value;
    if(value.length > amount){
      return value.length > 2 ? { invalidDrugsAmount: true } :null
    }
    return null
  };
}
